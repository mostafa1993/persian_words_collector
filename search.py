#!/usr/bin/python3

import fcntl
from grepfunc import grep
import os
import requests
from requests.exceptions import ConnectionError
from sys import argv, stdout, exit
from termcolor import colored
from multiprocessing import Process, Manager
from timeit import default_timer as time
import warnings
import yaml



class MyProcess(Process):
  err = None
  def run(self):
    try:
      Process.run(self)
    except SystemExit as err:
      self.err = err
    except BaseException:
      pass
    else:
      self.err = None


class Collection:
  """
  The Collection object contains collections of persian words and seach for new word to add them in its collections.
  Collections are verified, dehkhoda's, moeen's, suspicious and blacklist words.
  Args:
    dictionary --> contains various dictionaries links such as Dehkhoda and Moeen to check words based on them
    collection --> contains the file names of the aformentioned collections.
  """
  def __init__(self, dictionary, collection):
    self.__word_not_found = {}
    self.__dictionary = {}
    self.__doubt = 'آیا عبارت مورد نظر شما این بود'
    for dic_name, dic in dictionary.items():
      self.__word_not_found[dic_name] = dic['not_found'].split(',')
      self.__dictionary[dic_name] = dic['url']

    self.__collection = {}
    for key, name in collection.items():
      self.__collection[key] = {'file': name, 'list': Collection.__read(collection[key])}

    self.__collection['all'] = self.__collection['blacklist']['list'] + self.__collection['suspicious']['list'] + self.__collection['verified']['list']

    self.__word_is_blacklist = False
    self.__word_is_suspicious = False
    self.__word_is_verified = False
    self.__word_is_in = {'moeen': False, 'dehkhoda': False, 'more': False}

    self.__search_result = {'moeen': None, 'dehkhoda': None, 'more': None}
    self.manager = Manager()
    self.__search_result = self.manager.dict(self.__search_result)
    self.__processes = {}


  def finilize(self):
    alphabets = Collection.__read('alphabets.txt')
    alphabets = {c: i for i, c in enumerate(alphabets)}
    verified_file = self.__collection['verified']['file']
    dehkhoda_file = self.__collection['dehkhoda']['file']
    moeen_file = self.__collection['moeen']['file']
    verified = Collection.__read(verified_file)
    dehkhoda = Collection.__read(dehkhoda_file)
    moeen = Collection.__read(moeen_file)
    for file_name in (verified_file, dehkhoda_file, moeen_file):
      words_list = Collection.__read(file_name)
      sorted_list = sorted(words_list, key=lambda word: [alphabets.get(c, ord(c)) for c in word])
      with open(file_name, 'w') as f:
        f.write('\n'.join(sorted_list))


  @staticmethod
  def __read(file_name):
    """
    A method to read source file based on its name
    Args:
      file_name: source file name

    Returns:
      A list containing each line of the source content
    """
    if not os.path.exists(file_name):
      return []

    f = open(file_name, 'r')
    content = f.read().strip()
    content = content.split("\n")
    f.close()

    return content


  @staticmethod
  def polish(unpolished_file_name):
    """
    A method to remove unwanted charachters and sort alphabetically and write it to a file.
    Args:
     unpolished_file_name: the source file name

    Returns:
      A list containing each line of the polished source content
    """
    polished_file_name = 'polished_words.txt'
    if os.path.exists(polished_file_name):
      polished_content = Collection.__read(polished_file_name)
      return polished_content

    unpolished_file = Collection.__read(unpolished_file_name)
    alphabets = Collection.__read('alphabets.txt')
    words_collection = []
    for alphabet in alphabets:
      words = grep(unpolished_file, f"^{alphabet}")
      words = list(set(words))
      words.sort()
      words_collection += words

    polished_file = open(polished_file_name, 'w')
    words_collection_text = '\n'.join(words_collection)
    polished_file.write(words_collection_text)
    polished_file.close()

    return words_collection


  def __look_for(self, word, at, dic_name):
    """"""
    url = at.replace('QUERY', word)
    try:
      webpage = requests.get(url).text
    except Exception as error:
      raise SystemExit(type(error), url)

    self.__search_result[dic_name] = webpage


  def __find(self, word):
    """"""
    self.__found_duplicate = False
    if word in self.__collection['all']:
      self.__found_duplicate = True
      return

    if len(word) == 1:
      self.__word_is_blacklist = True
      return

    for dic_name, dictionary in self.__dictionary.items():
      look_for = MyProcess(target=self.__look_for, args=(word, dictionary, dic_name))
      self.__processes[dic_name] = look_for
      look_for.start()

    for dic_name, dictionary in self.__dictionary.items():
      word_is_not, word_not_found = self.__word_not_found[dic_name]
      process = self.__processes[dic_name]
      process.join(1)
      if not process.is_alive():
        if process.err:
          raise SystemExit(*process.err.args)

      process.join(10)
      if process.is_alive():
        print(colored('\n[Warning] Very low speed is detected. You may need to restart the run.', 'yellow'))
        process.join(20)
        if process.is_alive():
          print(colored('[Timeout error] The connection is too slow. Exiting...', 'red'))
          process.terminate()
          raise SystemExit(ConnectionError, dictionary.replace('QUERY', word))
        elif process.err:
          raise SystemExit(*process.err.args)

      result = self.__search_result[dic_name]
      self.__word_is_in[dic_name] = not(word_is_not in result or word_not_found in result or self.__doubt in result)

    self.__word_is_blacklist = not sum(self.__word_is_in.values())
    self.__word_is_verified = self.__word_is_in['dehkhoda'] or self.__word_is_in['moeen']
    self.__word_is_suspicious = not (self.__word_is_blacklist or self.__word_is_verified)


  def get_total_words(self):
    return len(self.__collection['all'])


  def collect(self, word):
    self.__find(word)
    if self.__found_duplicate:
      return

    if self.__word_is_blacklist:
      with open(self.__collection['blacklist']['file'], 'a') as f:
        f.write(f"{word}\n")

    elif self.__word_is_verified:
      with open(self.__collection['verified']['file'], 'a') as f:
        f.write(f"{word}\n")
      if self.__word_is_in['moeen']:
        with open(self.__collection['moeen']['file'], 'a') as f:
          f.write(f"{word}\n")
      if self.__word_is_in['dehkhoda']:
        with open(self.__collection['dehkhoda']['file'], 'a') as f:
          f.write(f"{word}\n")

    elif self.__word_is_suspicious:
      with open(self.__collection['suspicious']['file'], 'a') as f:
        f.write(f"{word}\n")

    else:
      # Must not reah here, otherwise something is wrong
      raise SystemExit(Exception, f'word {word} should be either in blacklist, verified or suspicious list, but that\'s not the case!!!')


def resume(step_back=119):
  """"""
  global last_saved_i
  try:
    with open('.saved_iteration_number', 'r') as f:
      try:
        last_saved_i = int(f.read().strip()) - step_back
        last_saved_i = last_saved_i if last_saved_i > 0 else 0
      except:
        last_saved_i = 0
  except FileNotFoundError:
    last_saved_i = 0

  global polished_words
  polished_words = enumerate(polished_words[last_saved_i:], last_saved_i)


def show_progress(i, start=None, so_far=None, error=None, url=None):
  """"""
  num_new_words = i - so_far
  stop = time()
  duration = stop - start
  speed = abs(round(60 * num_new_words/duration, 2))
  speed = f'({speed} words/min)' if duration > 2 else '\b'

  stdout.write('\r\x1b[2K')
  print(colored(f'{round(100*i/total_words, 2)} % completed {speed}.', 'green'), end="\r")
  if type(error) == type(None):
    return

  default_error_message = f'[Error] There was a unknown error while opening {url}. Please check it and run again.'
  error_message = {
      ConnectionError: f'[Error] There was a conncection error while opening \'{url}\' url. Please check your internet connection and run again.',
      KeyboardInterrupt: '[Error] Run is stopped due to keyboad interuption by user.',
      }
  error_message = colored(error_message.setdefault(error, default_error_message), 'red')

  num_new_words = i - so_far
  stop = time()
  duration = stop - start
  speed = round(60 * num_new_words/duration, 2)
  minutes, seconds = divmod(int(duration), 60)
  hours, minutes = divmod(minutes, 60)
  info_message = f'[Run Summary] Run time ~ {hours} h and {minutes} min and {seconds} sec ({speed} words/min)' \
                f' --> {num_new_words} new words and {i} words in total are investigated.'
  info_message = colored(info_message, 'yellow')
  print(f'\n{error_message}\n{info_message}')
  with open('.saved_iteration_number', 'w') as f:
    f.write(str(i))
  exit(1)


def lock_running(lpath):
  fd = None
  try:
    fd = os.open(lpath, os.O_CREAT)
    fcntl.flock(fd, fcntl.LOCK_NB | fcntl.LOCK_EX)
    return True
  except (OSError, IOError):
    if fd: os.close(fd)
    return False


if __name__ == '__main__':
  if not lock_running('/tmp/search_persian_words.lock'):
    print(colored('The script is already running.', 'red'))
    exit(1)

  unpolished_file_name = argv[1]
  with open('config.yaml', 'r') as f:
    config = yaml.safe_load(f)

  collection = Collection(dictionary=config['dictionary'], collection=config['collection'])
  polished_words = Collection.polish(unpolished_file_name)
  total_words = len(polished_words)
  so_far = collection.get_total_words()
  resume()

  start = time()
  old_time = 0
  for i, word in polished_words:
    new_time = time()
    if new_time - old_time > 5:
      show_progress(i, start, so_far)
      old_time = new_time
    
    try:
      collection.collect(word)
    except SystemExit as error:
      show_progress(i, start, so_far, error=error.args[0], url=error.args[1])
    except KeyboardInterrupt:
      show_progress(i, start, so_far, error=KeyboardInterrupt)

  collection.finilize()
  show_progress(i, start, so_far)
  print('\nAll words are investigated.')
