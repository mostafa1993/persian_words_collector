This project is aiming to validate and collect Persian words from any list containing words.
#How to run
first clone this repository,
```sh
git clone git@gitlab.com:mostafa1993/persian_words_collector.git
cd persian_words_collector
```

Then move your list inside the `persian_words_collector` directory and apply a basic polishment on your list by running
```sh
chmod +x basic_polish.sh
./basic_polish.sh YOUR_LIST
```

Now you will have a text file named `raw_words.txt`. (Ofcourse you can rename it)
Finally you may run the main code to search into `raw_words.txt` (or whatever you renamed it). If any new information is found,
it will be added to either blacklist, suspicious or moeen, dehkhoda and verified list. You just need to run as,
```sh
./search raw_words.txt
```

If for any reason the run is interupted either by `ctrl+c` or connection error or any other reason, your progressed is saved. Therefore, if you run the program again your progress will be resumed.

